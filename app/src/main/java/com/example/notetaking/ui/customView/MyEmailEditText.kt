package com.example.notetaking.ui.customView

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import androidx.core.widget.doOnTextChanged
import com.google.android.material.textfield.TextInputLayout

class MyEmailEditText : TextInputLayout {

    constructor(context: Context) : super(context) {
        init()
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        hint = "Email"
        textAlignment = View.TEXT_ALIGNMENT_VIEW_START
        errorIconDrawable = null
        editText?.doOnTextChanged { inputText, _, _, _ ->
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(inputText!!).matches()){
                error =  "Email tidak valid"
            }else{
                error = null
                isErrorEnabled = false
            }
        }
    }
    private fun init() {
    }
}