package com.example.notetaking.ui.login

import androidx.lifecycle.ViewModel
import com.example.notetaking.data.DataRepository
import com.example.notetaking.data.entity.User

class LoginViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {

    fun onLogin(username: String, password:String): User {
        return dataRepository.getLogin(username, password)
    }

}