package com.example.notetaking.ui.register

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.notetaking.R
import com.example.notetaking.data.entity.User
import com.example.notetaking.databinding.FragmentRegisterBinding
import com.example.notetaking.ui.ViewModelFactory
import com.google.android.material.textfield.TextInputLayout

class RegisterFragment : Fragment() {

    private lateinit var registerViewModel: RegisterViewModel
    private lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_register, container, false)

        val factory = ViewModelFactory.getInstance(requireContext())

        registerViewModel =
            ViewModelProvider(this, factory)[RegisterViewModel::class.java]

        binding.lifecycleOwner = this

        binding.registerViewModel = registerViewModel

        binding.btnRegister.isEnabled = false
        checkUsername()
        checkConfirmPassword()
        toLogin()

        onRegister()

        return binding.root
    }

    private fun onRegister(){
        checkButton()
        binding.btnRegister.setOnClickListener {
            val username = binding.edRegisterUsername.editText?.text.toString()
            val email = binding.edRegisterEmail.editText?.text.toString()
            val password = binding.edRegisterPassword.editText?.text.toString()
            registerViewModel.onRegister(User(username = username, email = email, password = password))
            findNavController().popBackStack()
        }
    }

    private fun checkUsername(){
        binding.edRegisterUsername.editText?.doOnTextChanged { inputText, _, _, _ ->
            binding.edRegisterUsername.errorIconDrawable = null
            if(inputText?.length!! < 6){
                binding.edRegisterUsername.error = "Minimal username harus 6 karakter"
            } else {
                binding.edRegisterUsername.error = null
                binding.edRegisterUsername.isErrorEnabled = false
            }
        }
    }

    private fun checkConfirmPassword(){
        binding.edRegisterConfirmPassword.errorIconDrawable = null
        binding.edRegisterConfirmPassword.endIconMode =
            TextInputLayout.END_ICON_PASSWORD_TOGGLE
        binding.edRegisterConfirmPassword.endIconDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.show_password_selector) as Drawable

        binding.edRegisterConfirmPassword.editText?.doOnTextChanged { inputText, _, _, _ ->
            val confirmPassword = binding.edRegisterPassword.editText?.text
            if(inputText?.length!! < 6){
                binding.edRegisterConfirmPassword.error = "Minimal password harus 6 karakter"
            } else {
                if (inputText.toString() != confirmPassword.toString()){
                    binding.edRegisterConfirmPassword.error = "Password tidak sama"
                }else{
                    binding.edRegisterConfirmPassword.error = null
                    binding.edRegisterConfirmPassword.isErrorEnabled = false
                }
            }

        }
    }

    private fun checkButton(){
        binding.edRegisterUsername.editText?.doOnTextChanged { _, _, _, _ ->
            if(!binding.edRegisterUsername.isErrorEnabled){
                binding.btnRegister.isEnabled = (binding.edRegisterEmail.editText?.text.toString().isNotEmpty()
                        && binding.edRegisterPassword.editText?.text.toString().isNotEmpty()
                        && binding.edRegisterConfirmPassword.editText?.text.toString().isNotEmpty())
            }else{
                binding.btnRegister.isEnabled = false
            }
        }
        binding.edRegisterEmail.editText?.doOnTextChanged { _, _, _, _ ->
            if(!binding.edRegisterEmail.isErrorEnabled){
                binding.btnRegister.isEnabled =
                    (binding.edRegisterUsername.editText?.text.toString().isNotEmpty()
                            && binding.edRegisterPassword.editText?.text.toString().isNotEmpty()
                            && binding.edRegisterConfirmPassword.editText?.text.toString().isNotEmpty())
            }else{
                binding.btnRegister.isEnabled = false
            }
        }
        binding.edRegisterPassword.editText?.doOnTextChanged { _, _, _, _ ->
            if(!binding.edRegisterPassword.isErrorEnabled){
                binding.btnRegister.isEnabled = (binding.edRegisterEmail.editText?.text.toString().isNotEmpty()
                        && binding.edRegisterUsername.editText?.text.toString().isNotEmpty()
                        && binding.edRegisterConfirmPassword.editText?.text.toString().isNotEmpty())
            }else{
                binding.btnRegister.isEnabled = false
            }
        }
        binding.edRegisterConfirmPassword.editText?.doOnTextChanged { _, _, _, _ ->
            if(!binding.edRegisterConfirmPassword.isErrorEnabled){
                binding.btnRegister.isEnabled = (binding.edRegisterEmail.editText?.text.toString().isNotEmpty()
                        && binding.edRegisterPassword.editText?.text.toString().isNotEmpty()
                        && binding.edRegisterUsername.editText?.text.toString().isNotEmpty())
            }else{
                binding.btnRegister.isEnabled = false
            }
        }
    }

    private fun toLogin(){
        binding.tvToLogin.setOnClickListener {
            findNavController().popBackStack()
        }
    }

}