package com.example.notetaking.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.notetaking.data.entity.Notes
import com.example.notetaking.databinding.ItemNotesBinding
import com.example.notetaking.ui.addNotesFragment.AddNotesFragment
import com.example.notetaking.ui.deleteNotesFragment.DeleteNotesFragment

class NotesAdapter(
    private val listNotes: List<Notes>,
    private val fragmentManager: FragmentManager
) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    class ViewHolder(var binding: ItemNotesBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemNotesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (_, title, notes) = listNotes[position]
        holder.binding.tvTitleNotes.text = title
        holder.binding.tvNotes.text = notes
        holder.binding.btnDelete.setOnClickListener {
            DeleteNotesFragment( listNotes[position]).show(fragmentManager, DeleteNotesFragment.TAG)
        }
        holder.binding.btnEdit.setOnClickListener {
            AddNotesFragment(true, listNotes[position]).show(fragmentManager, AddNotesFragment.TAG)
        }
    }

    override fun getItemCount(): Int  = listNotes.size

}