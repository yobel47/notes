package com.example.notetaking.ui.addNotesFragment

//noinspection SuspiciousImport
import android.R
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.notetaking.data.entity.Notes
import com.example.notetaking.databinding.AddNoteDialogBinding
import com.example.notetaking.ui.ViewModelFactory


class AddNotesFragment(private val onEditDialog: Boolean, private val notesData: Notes? = null) : DialogFragment() {

    private lateinit var binding: AddNoteDialogBinding
    private lateinit var addNotesViewModel: AddNotesViewModel
    private lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefName = "loginSharedPref"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE,
            R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, com.example.notetaking.R.layout.add_note_dialog, container, false)

        val factory = ViewModelFactory.getInstance(requireContext())

        binding.tvTittle.text = if(onEditDialog){"Edit Data"}else{"Input Data"}
        binding.btnAddNote.text = if(onEditDialog){"Update"}else{"Input"}
        if(onEditDialog){
            binding.edAddTitle.editText?.setText(notesData?.title)
            binding.edAddNotes.editText?.setText(notesData?.notes)
        }

        addNotesViewModel =
            ViewModelProvider(this, factory)[AddNotesViewModel::class.java]

        binding.lifecycleOwner = this

        binding.addNotesViewModel = addNotesViewModel

        sharedPreferences = requireActivity().getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)

        binding.btnAddNote.setOnClickListener {
                if(binding.edAddTitle.editText?.text.toString().isEmpty()){
                    binding.edAddTitle.editText?.requestFocus()
                    binding.edAddTitle.errorIconDrawable = null
                    binding.edAddTitle.error = "Harus ada isinya"
                    checkAddTitle()
                }else if(binding.edAddNotes.editText?.text.toString().isEmpty()){
                    binding.edAddNotes.editText?.requestFocus()
                    binding.edAddNotes.errorIconDrawable = null
                    binding.edAddNotes.error = "Harus ada isinya"
                    checkAddNotes()
                }else{
                    val userId = sharedPreferences.getInt("id",0)
                    val title = binding.edAddTitle.editText?.text.toString()
                    val notes = binding.edAddNotes.editText?.text.toString()
                    if(onEditDialog){
                        onEdit(notesData?.id!!,title,notes)
                    }else{
                        onAdd(userId,title,notes)
                    }
                }
            }

        return binding.root
    }

    private fun onAdd(userId: Int, title: String, notes: String){
        addNotesViewModel.onAddNotes(Notes(title = title, notes = notes, userId = userId))
        dismiss()
        Toast.makeText(context, "Simpan notes berhasil", Toast.LENGTH_SHORT).show()
    }

    private fun onEdit(noteId: Int, title: String, notes: String){
        addNotesViewModel.onEditNotes(title, notes, noteId)
        dismiss()
        Toast.makeText(context, "Update notes berhasil", Toast.LENGTH_SHORT).show()
    }


    private fun checkAddTitle(){
        binding.edAddTitle.editText?.doOnTextChanged { _, _, _, _ ->
            if(binding.edAddTitle.editText?.text.toString().isNotEmpty()){
                binding.edAddTitle.error = null
                binding.edAddTitle.isErrorEnabled =false
            }
        }
    }

    private fun checkAddNotes(){
        binding.edAddNotes.editText?.doOnTextChanged { _, _, _, _ ->
            if(binding.edAddNotes.editText?.text.toString().isNotEmpty()){
                binding.edAddNotes.error = null
                binding.edAddNotes.isErrorEnabled =false
            }
        }
    }

    companion object {
        const val TAG = "AddNotesFragment"
    }
}