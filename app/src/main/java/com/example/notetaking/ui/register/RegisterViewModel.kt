package com.example.notetaking.ui.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.notetaking.data.DataRepository
import com.example.notetaking.data.entity.User
import kotlinx.coroutines.launch

class RegisterViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {

    fun onRegister(user: User) {
        viewModelScope.launch {
            dataRepository.insertUser(user)
        }
    }

}