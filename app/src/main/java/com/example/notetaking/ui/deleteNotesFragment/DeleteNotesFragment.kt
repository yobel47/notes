package com.example.notetaking.ui.deleteNotesFragment

//noinspection SuspiciousImport
import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.notetaking.data.entity.Notes
import com.example.notetaking.databinding.DeleteNoteDialogBinding
import com.example.notetaking.ui.ViewModelFactory

class DeleteNotesFragment(private val notesData: Notes) : DialogFragment() {

    private lateinit var binding: DeleteNoteDialogBinding
    private lateinit var deleteNotesViewModel: DeleteNotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE,
            R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, com.example.notetaking.R.layout.delete_note_dialog, container, false)

        val factory = ViewModelFactory.getInstance(requireContext())

        deleteNotesViewModel =
            ViewModelProvider(this, factory)[DeleteNotesViewModel::class.java]

        binding.lifecycleOwner = this

        binding.deleteNotesViewModel = deleteNotesViewModel

        binding.btnDeleteNote.setOnClickListener {
            onDelete(notesData.id)
        }

        binding.btnCancel.setOnClickListener {
            dismiss()
        }

        return binding.root
    }

    private fun onDelete(notesId: Int){
        deleteNotesViewModel.onDeleteNotes(notesId)
        dismiss()
        Toast.makeText(context, "Hapus notes berhasil", Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val TAG = "DeleteNotesFragment"
    }
}