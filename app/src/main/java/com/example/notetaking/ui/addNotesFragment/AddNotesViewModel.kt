package com.example.notetaking.ui.addNotesFragment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.notetaking.data.DataRepository
import com.example.notetaking.data.entity.Notes
import kotlinx.coroutines.launch

class AddNotesViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {
    fun onAddNotes(notes: Notes) {
        viewModelScope.launch {
            dataRepository.insertNotes(notes)
        }
    }
    fun onEditNotes(title: String, notes: String, noteId: Int) {
        viewModelScope.launch {
            dataRepository.updateNotes(title, notes, noteId)
        }
    }
}