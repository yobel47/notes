package com.example.notetaking.ui.home

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notetaking.R
import com.example.notetaking.databinding.FragmentHomeBinding
import com.example.notetaking.ui.ViewModelFactory
import com.example.notetaking.ui.addNotesFragment.AddNotesFragment

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding
    private lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefName = "loginSharedPref"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false)

        var adapter : NotesAdapter

        val factory = ViewModelFactory.getInstance(requireContext())

        homeViewModel =
            ViewModelProvider(this, factory)[HomeViewModel::class.java]

        binding.lifecycleOwner = this

        binding.homeViewModel = homeViewModel

        sharedPreferences = requireActivity().getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)

        binding.username = sharedPreferences.getString("username",null)

        val userId = sharedPreferences.getInt("id",0)

        onLogout()

        homeViewModel.getAllNotes(userId).observe(viewLifecycleOwner) {
            if(it.isEmpty()){
                binding.tvRvEmpty.visibility = View.VISIBLE
            }else{
                binding.tvRvEmpty.visibility = View.INVISIBLE
            }
            adapter = NotesAdapter(it,childFragmentManager)
            binding.rvNotes.layoutManager = LinearLayoutManager(context)
            binding.rvNotes.setHasFixedSize(true)
            binding.rvNotes.adapter = adapter
        }

        binding.btnAddNote.setOnClickListener {
            AddNotesFragment(false, null).show(childFragmentManager,AddNotesFragment.TAG)
        }

        return binding.root
    }

    private fun onLogout(){
        binding.tvLogout.setOnClickListener {
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
            findNavController().navigate(R.id.loginFragment, null, NavOptions.Builder()
                .setPopUpTo(R.id.homeFragment, true)
                .build())
        }
    }
}