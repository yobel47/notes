package com.example.notetaking.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.notetaking.data.DataRepository
import com.example.notetaking.ui.addNotesFragment.AddNotesViewModel
import com.example.notetaking.ui.deleteNotesFragment.DeleteNotesViewModel
import com.example.notetaking.ui.home.HomeViewModel
import com.example.notetaking.ui.login.LoginViewModel
import com.example.notetaking.ui.register.RegisterViewModel

class ViewModelFactory(
    private val dataRepository: DataRepository
) : ViewModelProvider.Factory {
    companion object {
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance(context: Context): ViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: ViewModelFactory(
                    DataRepository.getInstance(context)
                )
            }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> {
                LoginViewModel(dataRepository) as T
            }
            modelClass.isAssignableFrom(RegisterViewModel::class.java) -> {
                RegisterViewModel(dataRepository) as T
            }
            modelClass.isAssignableFrom(HomeViewModel::class.java) -> {
                HomeViewModel(dataRepository) as T
            }
            modelClass.isAssignableFrom(AddNotesViewModel::class.java) -> {
                AddNotesViewModel(dataRepository) as T
            }
            modelClass.isAssignableFrom(DeleteNotesViewModel::class.java) -> {
                DeleteNotesViewModel(dataRepository) as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }
}