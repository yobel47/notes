package com.example.notetaking.ui.login

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.notetaking.R
import com.example.notetaking.databinding.FragmentLoginBinding
import com.example.notetaking.ui.ViewModelFactory

class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: FragmentLoginBinding
    private lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefName = "loginSharedPref"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_login, container, false)

        val factory = ViewModelFactory.getInstance(requireContext())

        loginViewModel =
            ViewModelProvider(this, factory)[LoginViewModel::class.java]

        binding.lifecycleOwner = this

        binding.loginViewModel = loginViewModel

        sharedPreferences = requireActivity().getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)

        binding.btnLogin.isEnabled = false

        checkAuth()
        toRegister()
        onLogin()

        return binding.root
    }

    private fun checkAuth(){
        val username = sharedPreferences.getString("username",null)
        if(username !== null){
            toHome()
        }
    }

    private fun onLogin(){
        checkButton()
        binding.btnLogin.setOnClickListener {
            val email = binding.edLoginEmail.editText?.text.toString()
            val password = binding.edLoginPassword.editText?.text.toString()
            val userData = loginViewModel.onLogin(email, password)
            if(userData!=null){
                val editor = sharedPreferences.edit()
                editor.putString("username",userData.username)
                editor.putInt("id",userData.id)
                editor.apply()
                toHome()
            }else{
                Toast.makeText(context, "Login gagal", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun toHome(){
        findNavController().navigate(R.id.homeFragment, null, NavOptions.Builder()
            .setPopUpTo(R.id.loginFragment, true)
            .build())
    }

    private fun checkButton(){
        binding.edLoginEmail.editText?.doOnTextChanged { _, _, _, _ ->
            if(!binding.edLoginEmail.isErrorEnabled){
                binding.btnLogin.isEnabled = (binding.edLoginPassword.editText?.text.toString().isNotEmpty())
            }else{
                binding.btnLogin.isEnabled = false
            }
        }
        binding.edLoginPassword.editText?.doOnTextChanged { _, _, _, _ ->
            if(!binding.edLoginPassword.isErrorEnabled){
                binding.btnLogin.isEnabled = (binding.edLoginEmail.editText?.text.toString().isNotEmpty())
            }else{
                binding.btnLogin.isEnabled = false
            }
        }
    }

    private fun toRegister(){
        binding.tvToRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

}