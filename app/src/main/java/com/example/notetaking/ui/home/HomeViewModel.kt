package com.example.notetaking.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.notetaking.data.DataRepository
import com.example.notetaking.data.entity.Notes

class HomeViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {

    fun getAllNotes(userId: Int): LiveData<List<Notes>> {
        return dataRepository.getAllNotes(userId)
    }

}