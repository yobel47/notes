package com.example.notetaking.ui.deleteNotesFragment

import androidx.lifecycle.ViewModel
import com.example.notetaking.data.DataRepository

class DeleteNotesViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {
    fun onDeleteNotes(noteId: Int) {
        dataRepository.deleteNotes(noteId)
    }
}