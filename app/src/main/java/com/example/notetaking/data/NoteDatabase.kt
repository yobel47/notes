package com.example.notetaking.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.notetaking.data.dao.NotesDao
import com.example.notetaking.data.dao.UserDao
import com.example.notetaking.data.entity.Notes
import com.example.notetaking.data.entity.User
import java.util.concurrent.Executors

@Database(
    entities = [User::class, Notes::class],
    version = 1,
    exportSchema = false
)
abstract class NoteDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun notesDao(): NotesDao

    companion object {
        @Volatile
        private var INSTANCE: NoteDatabase? = null
        fun getInstance(context: Context): NoteDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    NoteDatabase::class.java,
                    "notes"
                ).addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        Executors.newSingleThreadScheduledExecutor().execute {
                            INSTANCE!!.userDao()
                            INSTANCE!!.notesDao()
                        }
                    }
                }).allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }


}