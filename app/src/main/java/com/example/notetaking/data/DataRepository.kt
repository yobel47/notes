package com.example.notetaking.data

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.notetaking.data.dao.NotesDao
import com.example.notetaking.data.dao.UserDao
import com.example.notetaking.data.entity.Notes
import com.example.notetaking.data.entity.User

class DataRepository(
    private val userDao: UserDao,
    private val notesDao: NotesDao,
) {
    companion object {
        @Volatile
        private var instance: DataRepository? = null

        fun getInstance(context: Context): DataRepository {
            return instance ?: synchronized(this) {
                if (instance == null) {
                    val database = NoteDatabase.getInstance(context)
                    instance = DataRepository(
                        database.userDao(),
                        database.notesDao(),
                    )
                }
                return instance as DataRepository
            }
        }
    }

    //User
    fun insertUser(user: User): Long {
        return userDao.insertUser(user)
    }

    fun getLogin(username: String, password:String): User{
        return userDao.getUserByEmailPassword(username,password)
    }

    //Notes
    fun insertNotes(notes: Notes): Long {
        return notesDao.insertNotes(notes)
    }

    fun getAllNotes(userId: Int): LiveData<List<Notes>> {
        return notesDao.getAllNotesByUser(userId)
    }

    fun updateNotes(title: String, notes: String, noteId: Int){
        return notesDao.updateNotes(title, notes, noteId)
    }

    fun deleteNotes(noteId: Int){
        return notesDao.deleteNotes(noteId)
    }
}