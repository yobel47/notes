package com.example.notetaking.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.notetaking.data.entity.Notes

@Dao
interface NotesDao {
    @Insert
    fun insertNotes(notes: Notes): Long

    @Query("UPDATE notes_table SET title = :title, notes = :notes WHERE noteId = :noteId ")
    fun updateNotes(title: String, notes: String, noteId: Int)

    @Query("SELECT * from notes_table WHERE userId = :userId ORDER BY noteId ASC")
    fun getAllNotesByUser(userId: Int): LiveData<List<Notes>>

    @Query("DELETE FROM notes_table WHERE noteId = :noteId ")
    fun deleteNotes(noteId: Int)
}