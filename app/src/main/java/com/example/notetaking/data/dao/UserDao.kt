package com.example.notetaking.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.notetaking.data.entity.User

@Dao
interface UserDao {
    @Insert
    fun insertUser(user: User): Long

    @Query("SELECT * from user_table WHERE email = :email AND password = :password")
    fun getUserByEmailPassword(email: String, password: String): User
}